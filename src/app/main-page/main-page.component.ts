import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MainPageComponent implements OnInit {
  selectedInterview: string;
    states = [
      {name: 'Popova Sonya'},
      {name: 'Vasyliev Dmytro'},
      {name: 'Oleh Zalisskyi'},
      {name: 'Vitaliy Kalenichenko'},
      {name: 'Oleh Parfeniuk' },
      {name: 'Andrii Shramko' },
      {name: 'Sergii Sazonko' },
      {name: 'Taras Nikolaev'},
    ];
    constructor() { }
  ngOnInit() {
  }

}

